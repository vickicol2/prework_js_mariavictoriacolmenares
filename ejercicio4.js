// Crea una función que sea capaz de imprimir por consola todos los números comprendidos entre 1 9, y además encargándose de que se
// repita tantas veces como su número indique, es decir que el 7 aparecerá 7 veces.

for (var i=1; i<=9; i++){
    var result = "";
    for (var k=1; k<=i; k++){
        result += i.toString();
    }
    console.log(result);
}
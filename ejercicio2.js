// Debes crear una función que sepa clasificar ruedas de juguete, siempre y cuando le indiques el diámetro de la misma.
// En base al diámetro que reciba la función se comportará así:

// - 1) Si el diámetro es **inferior o igual** a 10, la función deberá imprimir por consola "es una rueda para un juguete pequeño"
// - 2) Si el diámetro es **superior** a 10 y **menor** de 20, la función deberá imprimir por consola "es una rueda para un juguete mediano"
// - 3) Si el diámetro es **superior o igual** a 20, la función deberá imprimir por consola "es una rueda para un juguete grande"

function clasificarRuedas(diametro){
    if (diametro<=10){
        console.log("Es una rueda para un juguete pequeño");
    } else if (diametro>10 && diametro<20){
        console.log("Es una rueda para un juguete mediano");
    } else if (diametro>=20) {
        console.log("Es una rueda para un juguete grande");
    }
}

clasificarRuedas(10);
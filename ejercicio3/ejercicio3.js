// Requisitos:
// 1. Solicite al usuario su número de DNI y su letra.
//    Debes realizar las preguntas por separado, es decir que tendrá que introducir dos datos, no uno. 
// 2. En primer lugar (y en una sola instrucción) se debe comprobar si el número es menor que `0` o mayor que `99999999`.
//    Si ese es el caso, se muestra un mensaje al usuario indicando que el número no es válido y el programa no muestra más mensajes.
// 3. Si el número es válido (paso), debes comprobar si la letra indicada por el usuario es correcta.
//    Para ello se sigue el siguiente método:
// El cálculo de la letra del Documento Nacional de Identidad (DNI) es un proceso matemático sencillo que se basa en obtener el resto de
// la división entera del número de DNI y el número 23. A partir del resto de la división, se obtiene la letra seleccionándola del array.
// Deberás imprimir por pantalla "DNI Válido" o "DNI Incorrecto".

var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var numeroDNI = parseInt(prompt("Ingrese su Número de DNI"));
var letraDNI = prompt("Ingrese su Letra de DNI");

if (numeroDNI<0 || numeroDNI>99999999){
    alert("El número no es válido");
} else {
    comprobarLetra(letraDNI);
}

function comprobarLetra(letraDNI){
    var resto = numeroDNI % 23;
    if (letraDNI == letras[resto]){
        alert("DNI Válido");
    } else {
        alert("DNI Incorrecto");
    }
}